# WWU Unset Mail Sender

* This module alters the mail header in emails sent by Drupal to unset the
  default sender field.
